import pytest
cnt = 0


@pytest.fixture()
def number_and_name_test():
    global cnt
    cnt += 1
    return f'Тест номер {cnt}'
