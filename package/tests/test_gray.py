import pytest
from gray import to_gray
from gray import is_gray


@pytest.mark.parametrize('color, result', [('#F0F0F0', (0, 0, 0)),
                                           ('#F0F0F1', (1, 1, 0)),
                                           ('#F0F2F0', (2, 0, 2)),
                                           ('#F5F0F3', (0, 5, 2)),
                                           ('#000000', (0, 0, 0))])
def test_to_gray(number_and_name_test, color, result):
    print(number_and_name_test)
    assert to_gray(color) == result


@pytest.mark.parametrize('color, result', [('#FOFOFO', True),
                                           ('#FOFOF1', False),
                                           ('#FOFOF2', False),
                                           ('#FOFOF3', False),
                                           ('#000000', True)])
def test_is_gray(number_and_name_test, color, result):
    print(number_and_name_test)
    assert is_gray(color) == result
