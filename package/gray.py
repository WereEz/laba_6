def is_gray(hex: str):
    """
    Проверит является ли цвет серым
    Возращаемое значение:
        True если цвет серый, иначе False
    """
    gray = '#' + hex[1:3]*3
    if hex == gray:
        return True
    else:
        return False


def to_gray(hex: str):
    """
    Определит сколько нужно добавить красного, зеленого и
    синего, чтобы цвет стал серым
    Возращаемое значение:
        rgb_dob (tuple)
    """
    rgb = [int(hex[1:3], 16), int(hex[3:5], 16), int(hex[5:], 16)]
    rgb_dop = tuple([int(max(rgb))-x for x in rgb])
    return rgb_dop
