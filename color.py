from package.gray import is_gray
from package.gray import to_gray
if __name__ == "__main__":
    hex = input('Введите шестнадцатеричный код ')
    while not (hex[0] == '#' and all(hex[i+1] in '0123456789ABCDEF' for i in range(len(hex)-1)) and len(hex) == 7):
        hex = input('Цвет должен быть записан шестнадцатеричным кодом ')
    if is_gray(hex):
        print('Это серый')
    else:
        print(f'Нужно добавить до серого - {to_gray(hex)}')
